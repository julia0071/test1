FROM nginx:latest
COPY cat.html /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf
